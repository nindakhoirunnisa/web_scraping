from selenium import webdriver
import pandas as pd
from webdriver_manager.chrome import ChromeDriverManager
import itertools
import time
import MySQLdb
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

HOST = "localhost"
USERNAME = "root"
PASSWORD = ""
DATABASE = "dataset"
iterasi = []
tambahan = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','aa','ab','ac','ad','ae','af','ag','ah']

def create_subset(t):
    r = []
    for L in range(0, t+1):
        for subset in itertools.combinations(range(1,t+1), L):
            if subset != ():
                r.append(subset)
    return r

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get("https://www.mayoclinic.org/symptom-checker/abdominal-pain-in-adults-adult/related-factors/itt-20009075")

pain_count = len(driver.find_elements_by_class_name("frm_options")[0].find_elements_by_tag_name("li"))
pain_located_count = len(driver.find_elements_by_class_name("frm_options")[1].find_elements_by_tag_name("li"))
triggered_count = len(driver.find_elements_by_class_name("frm_options")[2].find_elements_by_tag_name("li"))
relieved_count = len(driver.find_elements_by_class_name("frm_options")[3].find_elements_by_tag_name("li"))
accompanied_count = len(driver.find_elements_by_class_name("frm_options")[4].find_elements_by_tag_name("li"))

pain_checkbox = create_subset(pain_count)
location_checkbox = create_subset(pain_located_count)
trigger_checkbox = create_subset(triggered_count)
reliever_checkbox = create_subset(relieved_count)
accompanier_checkbox = create_subset(accompanied_count)

def get_data():
    Find = driver.find_element_by_xpath('//*[@id="FindCause"]')
    driver.execute_script("arguments[0].click();", Find)

    satu = driver.find_element_by_xpath('//*[@id="main_0_maincontent_1_RepeaterCause_causeDiv_0"]/h3/a').text
    dua = driver.find_element_by_xpath('//*[@id="main_0_maincontent_1_RepeaterCause_causeDiv_1"]/h3/a').text
    tiga = driver.find_element_by_xpath('//*[@id="main_0_maincontent_1_RepeaterCause_causeDiv_2"]/h3/a').text

    driver.implicitly_wait(0.1)

    cursor.execute("INSERT INTO tabel2004 (pain_is, pain_located_in, triggered_by, relieved_by, accompanied_by, diagnosa_1, diagnosa_2, diagnosa_3) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)", (['; '.join(pain_is)], ['; '.join(pain_located_in)], ['; '.join(triggered_by)], ['; '.join(relieved_by)], ['; '.join(accompanied_by)], satu, dua, tiga))
    db.commit()

    #cache
    iterasi.append('x')
    if(len(iterasi) == 30):
        iterasi.clear()
        driver.execute_script("window.open('about:blank','secondtab');")
        driver.switch_to.window("secondtab")
        driver.get('chrome://settings/clearBrowserData')
        time.sleep(2)
        actions = ActionChains(driver)
        actions.send_keys(Keys.TAB * 4 + Keys.SPACE * 1)
        actions.perform()
        actions.send_keys(Keys.TAB * 7 + Keys.SPACE * 1)
        actions.perform()
        iterasi.extend(tambahan)
        driver.switch_to.window(driver.window_handles[0])
    elif(len(iterasi) == 64):
        driver.switch_to.window("secondtab")
        driver.get('chrome://settings/clearBrowserData')
        time.sleep(2)
        aksi = ActionChains(driver)
        aksi.send_keys(Keys.TAB * 7 + Keys.SPACE * 1)
        aksi.perform()
        del iterasi[0:30]
        driver.switch_to.window(driver.window_handles[0])

    Back = driver.find_element_by_xpath('//*[@id="main_0_maincontent_1_LinkButton2"]')
    driver.execute_script("arguments[0].click();", Back)

for pain_option in pain_checkbox:
    db = MySQLdb.connect(HOST, USERNAME, PASSWORD, DATABASE)
    cursor = db.cursor()
    for number in pain_option:
        pain_is = []
        pain_click = driver.find_elements_by_class_name("frm_options")[0].find_elements_by_tag_name("li")[number - 1]
        driver.implicitly_wait(10)
        pain_click.click()

        for number in pain_option:
            pain_is_text = driver.find_elements_by_class_name("frm_options")[0].find_elements_by_tag_name("label")[number - 1].text
            pain_is.append(pain_is_text)

    for pain_location in location_checkbox:
        pain_located_in = []
        for number in pain_location:
            painloc_click = driver.find_elements_by_class_name("frm_options")[1].find_elements_by_tag_name("li")[number - 1]
            driver.implicitly_wait(10)
            painloc_click.click()
            for number in pain_location:
                pain_located_in_text = driver.find_elements_by_class_name("frm_options")[1].find_elements_by_tag_name("label")[number - 1].text
                pain_located_in.append(pain_located_in_text)

        for trigger_option in trigger_checkbox:
            triggered_by = []
            for number in trigger_option:
                trigger_click = driver.find_elements_by_class_name("frm_options")[2].find_elements_by_tag_name("li")[number - 1]
                driver.implicitly_wait(10)
                trigger_click.click()
                for number in trigger_option:
                    triggered_by_text = driver.find_elements_by_class_name("frm_options")[2].find_elements_by_tag_name("label")[number - 1].text
                    triggered_by.append(triggered_by_text)

            for reliever_option in reliever_checkbox:
                relieved_by = []
                for number in reliever_option:
                    reliever_click = driver.find_elements_by_class_name("frm_options")[3].find_elements_by_tag_name("Li")[number - 1]
                    driver.implicitly_wait(10)
                    reliever_click.click()
                    for number in reliever_option:
                        relieved_by_text = driver.find_elements_by_class_name("frm_options")[3].find_elements_by_tag_name("label")[number - 1].text
                        relieved_by.append(relieved_by_text)

                for accompanier_option in accompanier_checkbox:
                    for number in accompanier_option:
                        accompanier_click = driver.find_elements_by_class_name("frm_options")[4].find_elements_by_tag_name("input")[number - 1]
                        driver.implicitly_wait(10)
                        try:
                            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                            actions = ActionChains(driver)
                            actions.move_to_element(accompanier_click).perform()
                            driver.execute_script("arguments[0].click();", accompanier_click)
                            #accompanier_click.click()
                            accompanied_by = []
                            for number in accompanier_option:
                                accompanied_by_text = driver.find_elements_by_class_name("frm_options")[4].find_elements_by_tag_name("label")[number - 1].text
                                accompanied_by.append(accompanied_by_text)                                
                        except:
                            pass

                    kukis = driver.find_element_by_id("mayo-privacy-opt-in")
                    driver.execute_script("arguments[0].click();", kukis)

                    get_data()

                    for number in accompanier_option:
                        driver.find_elements_by_class_name("frm_options")[4].find_elements_by_tag_name("li")[number - 1].click()
                for number in reliever_option:
                    driver.find_elements_by_class_name("frm_options")[3].find_elements_by_tag_name("li")[number - 1].click()
            for number in trigger_option:
                driver.find_elements_by_class_name("frm_options")[2].find_elements_by_tag_name("li")[number - 1].click()
        for number in pain_location:
            driver.find_elements_by_class_name("frm_options")[1].find_elements_by_tag_name("li")[number - 1].click()
    for number in pain_option:
        driver.find_elements_by_class_name("frm_options")[0].find_elements_by_tag_name("li")[number - 1].click()
    
    cursor.close()

    """
    CREATE TABLE tabel2004 (
    id INT(14) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    pain_is VARCHAR(1000) NOT NULL,
    pain_located_in VARCHAR(1000) NOT NULL,
    triggered_by VARCHAR(1000) NOT NULL,
    relieved_by VARCHAR(1000) NOT NULL,
    accompanied_by VARCHAR(1000) NOT NULL,
    diagnosa_1 VARCHAR(44) NOT NULL,
    diagnosa_2 VARCHAR(44) NOT NULL,
    diagnosa_3 VARCHAR(44) NOT NULL
    )
    """